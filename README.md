# README #

This program can edit PixTudio/BennuGD control points embedded in PNG file metadata.

### Requirements ###

You'll need Python 3.6 with Qt5 and pyqt bindings for it to work.

### Screenshot ###

![Screenshot of the app](/img/screenshot.png)